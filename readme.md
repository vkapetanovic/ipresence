# iPresence Tech Test

## How to install the app

1. Clone the repository
1. Copy & paste .env.example and rename to .env
1. Open `.env` and set environment settings if necessary

## How to run the app
1. Execute `/v1/api/shout/steve-jobs` or 
`/v1/api/shout/steve-jobs&limit=N` where `N` is a number between 0 and 10.
