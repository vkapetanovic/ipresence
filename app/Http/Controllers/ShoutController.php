<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Services\QuotesService;
use Illuminate\Http\Request;

class ShoutController extends Controller
{
    public function show(Request $request, $person = NULL)
    {
        // return error when person name is missing
        if (!$person)
        {
            return response()
                ->json(['error' => true, 'message' => 'Person name missing.'], 400);
        }

        $limit = $request->query('limit') ?: 10;

        // return error when limit is out of boundaries
        if ($limit > 10 || $limit < 0)
        {
            return response()
                ->json(['error' => true, 'message' => 'Only 10 quotes per request allowed.'], 403);
        }

        // Load quotes
        $fileContent = file_get_contents(storage_path('sources/quotes.json'));
        $quotesJSON = json_decode($fileContent);
        $quotes =  collect($quotesJSON->quotes);

        // Filter quotes
        $quotesService = new QuotesService($quotes);
        $filteredQuotes = $quotesService->getQuoteByName($person, $limit);
        $filteredQuotes = $quotesService->prepareForDisplay($filteredQuotes);

        return response()
            ->json($filteredQuotes->pluck('quote'));
    }
}