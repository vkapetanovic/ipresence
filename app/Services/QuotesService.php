<?php

namespace App\Services;


use App\Helpers\GeneralHelper;
use Illuminate\Support\Facades\Cache;

class QuotesService
{
    private $quotes;

    private $cacheDuration = 60;

    public function __construct($quotes)
    {
        $this->quotes = $quotes;
    }

    /**
     * Get filtered quotes
     *
     * @param $filter
     * @param null $limit
     * @return mixed
     */
    public function getQuoteByName($filter, $limit = null)
    {
        // get from cache (and cache response)
        $cache_name = "quotes_" . $filter . "_" . $limit;
        $filteredQuotes = Cache::pull($cache_name, false);
        if (!$filteredQuotes) {
            $filteredQuotes = $this->quotes->filter(function ($quote) use ($filter) {
                return GeneralHelper::slug($quote->author) == GeneralHelper::slug(strtolower($filter));
            });

            if ($limit) {
                $filteredQuotes = $filteredQuotes->take($limit);
            }

            Cache::set($cache_name, $filteredQuotes, $this->cacheDuration);
        }
        return $filteredQuotes;
    }

    /**
     * Prepares output
     *
     * @param $filteredQuotes
     * @return mixed
     */
    public function prepareForDisplay($filteredQuotes)
    {
        $filteredQuotes = $filteredQuotes->map(function ($quote) {
                $quote->quote = mb_convert_encoding(strtoupper(GeneralHelper::rTrimSpecialChars($quote->quote)) . "!", "UTF-8");
            return $quote;
        });

        return $filteredQuotes;
    }
}