<?php

namespace App\Helpers;


class GeneralHelper
{
    /**
     * String slug
     *
     * @param $string
     * @return string
     */
    public static function slug($string)
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
    }

    /**
     * Trim chars
     *
     * @param $string
     * @return string
     */
    public static function rTrimSpecialChars($string)
    {
        $string = rtrim($string, ".");
        $string = rtrim($string, "!");
        $string = rtrim($string, "?");
        $string = rtrim($string, ",");
        return $string;
    }
}