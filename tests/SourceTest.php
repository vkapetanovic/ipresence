<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class SourceTest extends TestCase
{
    public $file;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->file = storage_path('sources/quotes.json');
    }

    /**
     * Test if source is valid
     *
     * @return void
     */
    public function testSourceExists()
    {
        // test file existance
        $file_exists = file_exists($this->file);
        $this->assertEquals(true, $file_exists);
    }

    /**
     * Test if source has content
     *
     * @return bool|string
     */
    public function testSourceHasContent()
    {
        // test if has content
        $content = file_get_contents($this->file);
        $this->assertEquals(false, empty($content));

        return $content;
    }

    /**
     * Test if "quotes" exists
     *
     * @depends testSourceHasContent
     * @return mixed
     */
    public function testIfQuotesExist($content)
    {
        // test file content
        $quotesJson = json_decode($content);
        $this->assertEquals(true, isset($quotesJson->quotes));

        return $quotesJson;
    }

}
