<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class SearchV1Test extends TestCase
{
    public $url = 'v1/api/shout/';

    /**
     * A basic test
     *
     * @return void
     */
    public function testSingleResponse()
    {
        $response = $this->call('GET', $this->url . 'Steve Jobs', ['limit' => 1]);

        $shouts = collect($response->getContent());

        $this->assertEquals(1, $shouts->count());
    }

    /**
     * Test more than 10 shouts
     *
     * @return void
     */
    public function testMoreThanTen()
    {
        $response = $this->call('GET', $this->url . 'Steve Jobs', ['limit' => 11]);

        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * Test empty name
     *
     * @return void
     */
    public function testEmptyName()
    {
        $response = $this->call('GET', $this->url);

        $this->assertEquals(400, $response->getStatusCode());
    }
}
