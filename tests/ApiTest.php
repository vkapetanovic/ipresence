<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ApiTest extends TestCase
{
    /**
     * A basic API test.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/v1/api');

        $this->assertEquals(
            'v1/api', $this->response->getContent()
        );
    }
}
